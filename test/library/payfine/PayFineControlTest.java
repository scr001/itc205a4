package library.payfine;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import library.entities.IBook;
import library.entities.ICalendar;
import library.entities.ILibrary;
import library.entities.ILoan;
import library.entities.IPatron;
import library.entities.Library;
import library.entities.Loan;
import library.entities.helpers.BookHelper;
import library.entities.helpers.CalendarFileHelper;
import library.entities.helpers.LoanHelper;
import library.entities.helpers.PatronHelper;
import library.returnbook.IReturnBookControl;
import library.returnbook.IReturnBookUI;
import library.returnbook.ReturnBookControl;
import library.returnbook.ReturnBookUI;
import library.returnbook.IReturnBookUI.UIStateConstants;

class PayFineControlTest {

	ILibrary library;
	ILoan loan;
	IBook book;
	IPatron patron;
	IReturnBookControl returnBookControl;
	IReturnBookUI returnBookUI;
	UIStateConstants uiState;
	ICalendar calendar;
	CalendarFileHelper calendarHelper;
	
	@BeforeEach
	void setUp() throws Exception {
		library = new Library(new BookHelper(), new PatronHelper(), new LoanHelper());
		calendarHelper = new CalendarFileHelper();
		calendar = calendarHelper.loadCalendar();
		patron = library.addPatron("LN","FN","Email",1111);
		book = library.addBook("A","T","CN");
		loan = new Loan(book,patron);
		library.commitLoan(loan);
		returnBookControl = new ReturnBookControl(library);
		returnBookUI = new ReturnBookUI(returnBookControl);
	}

	@AfterEach
	void tearDown() throws Exception {
	}

	@Test
	void testPatronTotalFineWhenLoanOverdueByOneDay() {
		int bookId = 1;
		boolean isDamaged = false;
		double expected = 1.0;
		int days = 3;
		//Act
		calendar.incrementDate(days);
		library.checkCurrentLoansOverDue();
		returnBookControl.bookScanned(bookId);
		returnBookControl.dischargeLoan(isDamaged);
		double actual = patron.getFinesPayable();
		//Assert
		
		assertEquals(expected, actual);
	}

}

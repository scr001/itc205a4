package library.entities;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

import java.util.Date;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import library.entities.helpers.BookHelper;
import library.entities.helpers.LoanHelper;
import library.entities.helpers.PatronHelper;

class LibraryTest {
	ILibrary library;
	@Mock ILoan mockLoan;
	private static Date date;
	java.util.Calendar cal;
	@BeforeEach
	void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		library = new Library (new BookHelper(), new PatronHelper(), new LoanHelper());
	}

	@AfterEach
	void tearDown() throws Exception {
	}

	@Test
	void testCalculateOverDueFineByOneDay() {
		//Arrange
		//due date is set to be yesterday (1 days overdue)
		cal = java.util.Calendar.getInstance();
		cal.set(java.util.Calendar.HOUR_OF_DAY, 0);
		cal.set(java.util.Calendar.MINUTE, 0);
		cal.set(java.util.Calendar.SECOND, 0);
		cal.set(java.util.Calendar.MILLISECOND, 0);
		cal.add(cal.DATE, -1);
		date = cal.getTime();
		when(mockLoan.isOverDue()).thenReturn(true);
		when(mockLoan.getDueDate()).thenReturn(date);
		//Act
		double expected = 1.0;
		double actual = library.calculateOverDueFine(mockLoan);
		//Assert
		assertEquals(expected, actual);
	}
	

}
